package ejemplo1;

import java.lang.reflect.Field;

public class Ejemplo1 {

	public static void main(String[] args) throws ClassNotFoundException {
		Class<?> clazz = Class.forName("ejemplo1.Cliente");
		Field[] campos = clazz.getDeclaredFields();
		for (Field campo : campos) {
			System.out.println(campo.getName() + " de tipo " + campo.getType().getName());
		}
	}

}
