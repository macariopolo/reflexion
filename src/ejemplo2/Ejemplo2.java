package ejemplo2;

import java.lang.reflect.Field;
import java.util.Scanner;

public class Ejemplo2 {

	public static void main(String[] args) throws ClassNotFoundException {
		Class<?> clazz=null;
		try(Scanner teclado = new Scanner(System.in)) {
			System.out.println("Escribe el nombre completo de una clase (ej.: java.lang.String, java.util.Vector...): ");
			String nombreClase = teclado. nextLine();
			
			clazz = Class.forName(nombreClase);
			
			Field[] campos = clazz.getDeclaredFields();
			for (Field campo : campos) {
				System.out.println(campo.getName() + " de tipo " + campo.getType().getName());
			}
		}
	}

}
