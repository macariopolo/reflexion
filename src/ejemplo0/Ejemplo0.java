package ejemplo0;

public class Ejemplo0 {
	public static void main(String[] args) throws ClassNotFoundException {
		Class<?> clase = Class.forName("ejemplo0.Cliente");
		
		System.out.println("clase.getName(): " + clase.getName());
		System.out.println("clase.getSimpleName(): " + clase.getSimpleName());
		System.out.println("clase.getCanonicalName(): " + clase.getCanonicalName());
	}
}
