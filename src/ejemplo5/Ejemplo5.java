package ejemplo5;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class Ejemplo5 {
	public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchFieldException, SecurityException, NoSuchMethodException, IllegalArgumentException, InvocationTargetException {
		Class<?> clazzFactura = Factura.class;
		Constructor<?> constructor = clazzFactura.getDeclaredConstructor(String.class, Cliente.class);

		Class<?> clazzPF = Class.forName("ejemplo5.PersonaFisica");
		Object personaFisica = clazzPF.newInstance(); 
				
		Field campoNombre = clazzPF.getDeclaredField("nombre");
		campoNombre.setAccessible(true);		
		campoNombre.set(personaFisica, "Fulano");	
		
		Field campoApellidos = clazzPF.getDeclaredField("apellidos");
		campoApellidos.setAccessible(true);
		campoApellidos.set(personaFisica, "de Tal");
		
		Field campoNIF = findField("nif", clazzPF);
		campoNIF.setAccessible(true);
		campoNIF.set(personaFisica,  "00123456J");
		
		Object factura = constructor.newInstance("00001", personaFisica);
		
		System.out.println(factura.toString());
	}

	private static Field findField(String nombreDelCampo, Class<?> clazz) {
		Class<?> superclass = clazz;
		Field campo=null;
		while ((superclass = superclass.getSuperclass())!=null) {
			try {
				campo = superclass.getDeclaredField(nombreDelCampo);
				break;
			} catch (NoSuchFieldException e) {
			} catch (SecurityException e) {
			}
		}
		return campo;
	}
}
