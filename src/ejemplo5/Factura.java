package ejemplo5;

public class Factura {
	private String numero;
	private Cliente cliente;
	
	public Factura(String numero, Cliente cliente) {
		super();
		this.numero = numero;
		this.cliente = cliente;
	}
	
	public void cambiarDatos(String nuevoNumero, Cliente nuevoCliente) {
		this.numero=nuevoNumero;
		this.cliente=nuevoCliente;
	}
	
	@Override
	public String toString() {
		return "Factura número " + numero + 
				", del cliente: " + cliente.toString();
	}
}
