package ejemplo5;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Ejemplo5B {
	public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchFieldException, SecurityException, NoSuchMethodException, IllegalArgumentException, InvocationTargetException {
		Class<?> clazzFactura = Factura.class;
		Constructor<?> constructor = clazzFactura.getDeclaredConstructor(String.class, Cliente.class);

		PersonaFisica fulano = new PersonaFisica("Fulano", "de Tal", "00123456J");
		PersonaFisica mengano = new PersonaFisica("Mengano", "de Cual", "987654321P");
		
		Object factura = constructor.newInstance("00001", fulano);
		System.out.println(factura.toString());
		
		Method metodo = clazzFactura.getDeclaredMethod("cambiarDatos", String.class, Cliente.class);
		metodo.invoke(factura, "0002", mengano);
		
		System.out.println(factura.toString());
	}
}
