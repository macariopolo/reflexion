package ejemplo5;

public class PersonaFisica extends Cliente {
	private String nombre;
	private String apellidos;
	
	public PersonaFisica() {
		super();
	}
	
	public PersonaFisica(String nombre, String apellidos, String nif) {
		super(nif);
		this.nombre = nombre;
		this.apellidos = apellidos;
	}

	@Override
	public String toString() {
		return nombre + " " + apellidos + ", con NIF " + super.toString();
	}
}
