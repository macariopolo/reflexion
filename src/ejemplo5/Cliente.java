package ejemplo5;

public abstract class Cliente {
	private String nif;

	public Cliente() {
	}

	public Cliente(String nif) {
		this.nif = nif;
	}

	@Override
	public String toString() {
		return nif;
	}
}
