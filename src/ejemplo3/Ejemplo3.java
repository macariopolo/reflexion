package ejemplo3;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Ejemplo3 {

	public static void main(String[] args) throws ClassNotFoundException {
		Class<?> clazz=null;
		try(Scanner teclado = new Scanner(System.in)) {
			System.out.println("Escribe el nombre completo de una clase (ej.: ejemplo3.PersonaFisica...): ");
			String nombreClase = teclado. nextLine();
			
			clazz = Class.forName(nombreClase);
			
			Field[] campos = clazz.getDeclaredFields();
			System.out.println("Campos declarados: ");
			for (Field campo : campos) {
				System.out.println(campo.getName() + " de tipo " + campo.getType().getName());
			}
			
			System.out.println("Campos heredados: ");
			List<Field> camposHeredados = getInheritedFields(clazz);
			for (Field campo : camposHeredados) {
				System.out.println(campo.getName() + " de tipo " + campo.getType().getName());
			}
		}
	}

	private static List<Field> getInheritedFields(Class<?> clazz) {
		ArrayList<Field> campos = new ArrayList<>();
		Class<?> superclass = clazz;
		while ((superclass=superclass.getSuperclass())!=null) {
			Field[] camposSuperclase = superclass.getDeclaredFields();
			campos.addAll(Arrays.asList(camposSuperclase));
		}
		return campos;
	}

}
