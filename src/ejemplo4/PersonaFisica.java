package ejemplo4;

public class PersonaFisica extends Cliente {
	private String nombre;
	private String apellidos;
	
	@Override
	public String toString() {
		return nombre + " " + apellidos;
	}
}
