package ejemplo4;

import java.lang.reflect.Field;

public class Ejemplo4 {
	public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchFieldException, SecurityException {
		Class<?> clazz = Class.forName("ejemplo4.PersonaFisica");
		
		Object objeto = clazz.newInstance();
		
		Field campoNombre = clazz.getDeclaredField("nombre");
		campoNombre.setAccessible(true);
		campoNombre.set(objeto, "Fulano");	
		
		Field campoApellidos = clazz.getDeclaredField("apellidos");
		campoApellidos.setAccessible(true);
		campoApellidos.set(objeto, "de Tal");
		
		System.out.println(objeto.toString()); 
	}
}
