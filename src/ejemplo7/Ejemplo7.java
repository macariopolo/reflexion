package ejemplo7;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;

public class Ejemplo7 {

	public static void main(String[] args) throws IOException {
		preparar(Ejemplo7.class);
	}

	private static void preparar(Class<?> clazz) throws IOException {
		String packageName = clazz.getPackage().getName();
		System.out.println(packageName);
		
		String directorio = new File(".").getAbsolutePath();
		System.out.println(directorio);
		directorio = directorio.substring(0, directorio.length()-1);
		System.out.println(directorio);
		directorio = directorio + "bin/" + packageName + File.separatorChar;
		System.out.println(directorio);
		
		List<File> files = (List<File>) FileUtils.listFiles(new File(directorio), TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
		for (File file : files) {
			System.out.println("file: " + file.getCanonicalPath());
		}
	}

}
