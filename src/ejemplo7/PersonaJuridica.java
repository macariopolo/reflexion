package ejemplo7;

public class PersonaJuridica extends Cliente {
	private String razonSocial;
	
	public PersonaJuridica(String razonSocial, String nif) {
		super(nif);
		this.razonSocial = razonSocial;
	}
	
	@Override
	public String toString() {
		return razonSocial + ", con NIF " + super.toString();
	}
}
