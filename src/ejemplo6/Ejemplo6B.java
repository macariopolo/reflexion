package ejemplo6;

import java.util.List;

public class Ejemplo6B {

	public static void main(String[] args) throws Exception {
		PersonaFisica pepe = new PersonaFisica("Pepe", "Pérez", "12345A");
		PersonaFisica ana = new PersonaFisica("Ana", "López", "6789B");
		
		PersonaJuridica uclm = new PersonaJuridica("UCLM", "Q1368009E");
		PersonaJuridica empresaFalsa = new PersonaJuridica("Otra", "6789B");
		
		TablaSQL<String, Cliente> tabla = new TablaSQL<>();
		tabla.insert(pepe);
		tabla.insert(ana);
		tabla.insert(uclm);
		
		try {
			tabla.insert(empresaFalsa);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		List<Cliente> listado = tabla.selectAll();
		for (Cliente cliente : listado)
			System.out.println(cliente.toString());
	}

}
