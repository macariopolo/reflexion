package ejemplo6;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SQLizador {
	public static String getCreate(Object objeto) throws Exception {
		Class<?> clazz = objeto.getClass();
		
		if (!clazz.isAnnotationPresent(Tabla.class))
			throw new Exception("La clase del objeto no es una Tabla");
		
		List<Field> campos = getCampos(clazz);
		String sql = "select ";
		for (Field campo : campos)
			sql = sql + campo.getName() + ", ";
		sql = sql.substring(0, sql.length()-2);
		
		sql = sql + " from " + clazz.getSimpleName() + " where ";
		
		Field campo = findIndiceUnico(campos);
		campo.setAccessible(true);
		Object valor = campo.get(objeto);
		
		sql = sql + campo.getName() + " = " + valor.toString();
		
		return sql;
	}

	private static Field findIndiceUnico(List<Field> campos) {
		for (Field campo : campos)
			if (campo.isAnnotationPresent(IndiceUnico.class))
				return campo;
		return null;
	}

	private static List<Field> getCampos(Class<?> clazz) {
		List<Field> campos = new ArrayList<>();
		campos.addAll(Arrays.asList(clazz.getDeclaredFields()));
		campos.addAll(getCamposHeredados(clazz));
		return campos;
	}

	private static List<Field> getCamposHeredados(Class<?> clazz) {
		List<Field> campos = new ArrayList<>();
		Class<?> superclass = clazz.getSuperclass();
		while (superclass != null) {
			campos.addAll(Arrays.asList(superclass.getDeclaredFields()));
			superclass=superclass.getSuperclass();
		}
		return campos;
	}
}
