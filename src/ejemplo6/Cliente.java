package ejemplo6;

@Tabla
public class Cliente {
	@IndiceUnico
	private String nif;
	
	public Cliente(String nif) {
		this.nif = nif;
	}

	@Override
	public String toString() {
		return nif;
	}
}
