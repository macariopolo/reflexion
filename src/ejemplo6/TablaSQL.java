package ejemplo6;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

public class TablaSQL<CLAVE, TIPO> {
	private HashMap<CLAVE, TIPO> objetos;
	
	public TablaSQL() {
		this.objetos=new HashMap<>();
	}
	
	public void insert(TIPO objeto) throws Exception {
		if (!objeto.getClass().isAnnotationPresent(Tabla.class))
			throw new Exception(objeto.getClass() + " no es una tabla");
		Field campoIndice = findCampoIndice(objeto);
		if (campoIndice==null)
			throw new Exception("El objeto no tiene clave");
		
		Class<?> clazz = objeto.getClass();
		CLAVE valor = (CLAVE) campoIndice.get(objeto);
		
		if (objetos.get(valor)!=null)
			throw new Exception("Clave " + valor + " duplicada");
		objetos.put(valor, objeto);
	}
	
	public List<TIPO> selectAll() {
		List<TIPO> result = new ArrayList<>();
		Iterator<Entry<CLAVE, TIPO>> all = objetos.entrySet().iterator();
		while (all.hasNext())
			result.add(all.next().getValue());
		return result;
	}

	private Field findCampoIndice(TIPO objeto) {
		Class<?> superclass = objeto.getClass();
		while ((superclass = superclass.getSuperclass())!=null) {
			for (Field campo : superclass.getDeclaredFields()) {
				if (campo.isAnnotationPresent(IndiceUnico.class)) {
					campo.setAccessible(true);
					return campo;
				}
			}
		}
		return null;
	}
}
