package ejemplo6;

public class Ejemplo6A {

	public static void main(String[] args) throws Exception {
		PersonaFisica pepe = new PersonaFisica("Pepe", "Pérez", "12345A");
		
		PersonaJuridica uclm = new PersonaJuridica("UCLM", "Q1368009E");

		System.out.println(SQLizador.getCreate(pepe));
		
		System.out.println(SQLizador.getCreate(uclm));
	}

}
